﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerInput : MonoBehaviour, GameControls.IGameActions
{
 
    

    private GameControls controls = null;
    private Actor actor;

    private void Awake()
    {
        actor = GetComponent<Actor>();
        if (!actor) return;

        controls = new GameControls();
        controls.Game.SetCallbacks(this);
    }

    public void OnEnable()
    {
        if (controls == null) return;
        controls.Game.Enable();
    }

    public void OnDisable()
    {
        if (controls == null) return;
        controls.Game.Disable();
    }
    

    public void OnJump(InputAction.CallbackContext context)
    {
        if (context.canceled) return;
        actor.Jump();
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        actor.Move(context.ReadValue<float>());
    }

    public void OnShoot(InputAction.CallbackContext context)
    {
        if (context.canceled) return;
        actor.Shoot();
    }

    public void OnSelectweapon1(InputAction.CallbackContext context)
    {
        if(context.canceled) return;
        actor.SelectWeapon(0);
    }

    public void OnSelectweapon2(InputAction.CallbackContext context)
    {
        if(context.canceled) return;
        actor.SelectWeapon(1);
    }

    public void OnSelectweapon3(InputAction.CallbackContext context)
    {
        if (context.canceled) return;
        actor.SelectWeapon(2);
    }

    private void Aim(Vector3 cursorPos)
    {
        GameObject cam = GameObject.Find("Camera");
        if (cam != null)
        {
            Camera camComp = cam.GetComponent<Camera>();
            if (camComp != null)
            {
                Vector3 pos = camComp.WorldToScreenPoint(gameObject.transform.position);
                pos.z = 0;
                Vector3 direction = cursorPos - pos;
                actor.Aim(direction.normalized);
            }
        }
    }

    public void OnAim(InputAction.CallbackContext context)
    {
        if (context.canceled) return;
        Aim(context.ReadValue<Vector2>());
    }

    private void Update()
    {
        Aim(controls.Game.Aim.ReadValue<Vector2>());
    }

    public void OnMenu(InputAction.CallbackContext context)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
