// GENERATED AUTOMATICALLY FROM 'Assets/Input/GameControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @GameControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @GameControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""GameControls"",
    ""maps"": [
        {
            ""name"": ""Game"",
            ""id"": ""dd86f73c-cca0-4e8e-8352-2bdd7d9fb511"",
            ""actions"": [
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""2e1ef301-7e7c-4460-a9eb-aa9085dc02c7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Move"",
                    ""type"": ""Button"",
                    ""id"": ""c504acd5-d809-4bbe-894a-961e2012e10e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Shoot"",
                    ""type"": ""Button"",
                    ""id"": ""cf25567e-f613-45fd-9345-4fa55dc2e74a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Select weapon 1"",
                    ""type"": ""Button"",
                    ""id"": ""eafab5b5-c202-42d2-96de-5da55e8d685e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Select weapon 2"",
                    ""type"": ""Button"",
                    ""id"": ""474175c1-967b-4eaa-b84c-3e00a73f5d47"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Select weapon 3"",
                    ""type"": ""Button"",
                    ""id"": ""de33d532-af99-4a58-a8f1-3ef12d81f3fb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Aim"",
                    ""type"": ""Value"",
                    ""id"": ""0268fbee-d29c-4702-a722-1d2485b54e93"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Menu"",
                    ""type"": ""Button"",
                    ""id"": ""23a5497d-768d-47ff-9cd5-7f56f42aa00c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""7865a0fa-a945-4f3e-808d-e5adac67ca4d"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c861fe2a-df18-4b45-9ef6-151a40508042"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Direction"",
                    ""id"": ""2637dc7d-77eb-4326-8540-5f214e574cba"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""9b0e9639-809c-4eac-a8c0-ec17e2d90797"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""db056877-d1c1-49a6-bb97-2acdc85aa63e"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""789322ed-6a6c-4e88-9cb4-ef57332d8097"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Shoot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""de8d4655-3f96-46e4-8e02-b23e32ec37d9"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select weapon 1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2071b3d1-43be-499f-8323-794f3378d998"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select weapon 2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6c9a8126-9bee-44d3-b3a9-969e2466e5d6"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select weapon 3"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3b581965-c3d2-4fe1-875a-56645da3f2d8"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Aim"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5e78b027-16e0-4533-8b3a-0bce0b8f31f0"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Menu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Game
        m_Game = asset.FindActionMap("Game", throwIfNotFound: true);
        m_Game_Jump = m_Game.FindAction("Jump", throwIfNotFound: true);
        m_Game_Move = m_Game.FindAction("Move", throwIfNotFound: true);
        m_Game_Shoot = m_Game.FindAction("Shoot", throwIfNotFound: true);
        m_Game_Selectweapon1 = m_Game.FindAction("Select weapon 1", throwIfNotFound: true);
        m_Game_Selectweapon2 = m_Game.FindAction("Select weapon 2", throwIfNotFound: true);
        m_Game_Selectweapon3 = m_Game.FindAction("Select weapon 3", throwIfNotFound: true);
        m_Game_Aim = m_Game.FindAction("Aim", throwIfNotFound: true);
        m_Game_Menu = m_Game.FindAction("Menu", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Game
    private readonly InputActionMap m_Game;
    private IGameActions m_GameActionsCallbackInterface;
    private readonly InputAction m_Game_Jump;
    private readonly InputAction m_Game_Move;
    private readonly InputAction m_Game_Shoot;
    private readonly InputAction m_Game_Selectweapon1;
    private readonly InputAction m_Game_Selectweapon2;
    private readonly InputAction m_Game_Selectweapon3;
    private readonly InputAction m_Game_Aim;
    private readonly InputAction m_Game_Menu;
    public struct GameActions
    {
        private @GameControls m_Wrapper;
        public GameActions(@GameControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Jump => m_Wrapper.m_Game_Jump;
        public InputAction @Move => m_Wrapper.m_Game_Move;
        public InputAction @Shoot => m_Wrapper.m_Game_Shoot;
        public InputAction @Selectweapon1 => m_Wrapper.m_Game_Selectweapon1;
        public InputAction @Selectweapon2 => m_Wrapper.m_Game_Selectweapon2;
        public InputAction @Selectweapon3 => m_Wrapper.m_Game_Selectweapon3;
        public InputAction @Aim => m_Wrapper.m_Game_Aim;
        public InputAction @Menu => m_Wrapper.m_Game_Menu;
        public InputActionMap Get() { return m_Wrapper.m_Game; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameActions set) { return set.Get(); }
        public void SetCallbacks(IGameActions instance)
        {
            if (m_Wrapper.m_GameActionsCallbackInterface != null)
            {
                @Jump.started -= m_Wrapper.m_GameActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnJump;
                @Move.started -= m_Wrapper.m_GameActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnMove;
                @Shoot.started -= m_Wrapper.m_GameActionsCallbackInterface.OnShoot;
                @Shoot.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnShoot;
                @Shoot.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnShoot;
                @Selectweapon1.started -= m_Wrapper.m_GameActionsCallbackInterface.OnSelectweapon1;
                @Selectweapon1.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnSelectweapon1;
                @Selectweapon1.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnSelectweapon1;
                @Selectweapon2.started -= m_Wrapper.m_GameActionsCallbackInterface.OnSelectweapon2;
                @Selectweapon2.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnSelectweapon2;
                @Selectweapon2.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnSelectweapon2;
                @Selectweapon3.started -= m_Wrapper.m_GameActionsCallbackInterface.OnSelectweapon3;
                @Selectweapon3.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnSelectweapon3;
                @Selectweapon3.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnSelectweapon3;
                @Aim.started -= m_Wrapper.m_GameActionsCallbackInterface.OnAim;
                @Aim.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnAim;
                @Aim.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnAim;
                @Menu.started -= m_Wrapper.m_GameActionsCallbackInterface.OnMenu;
                @Menu.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnMenu;
                @Menu.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnMenu;
            }
            m_Wrapper.m_GameActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Shoot.started += instance.OnShoot;
                @Shoot.performed += instance.OnShoot;
                @Shoot.canceled += instance.OnShoot;
                @Selectweapon1.started += instance.OnSelectweapon1;
                @Selectweapon1.performed += instance.OnSelectweapon1;
                @Selectweapon1.canceled += instance.OnSelectweapon1;
                @Selectweapon2.started += instance.OnSelectweapon2;
                @Selectweapon2.performed += instance.OnSelectweapon2;
                @Selectweapon2.canceled += instance.OnSelectweapon2;
                @Selectweapon3.started += instance.OnSelectweapon3;
                @Selectweapon3.performed += instance.OnSelectweapon3;
                @Selectweapon3.canceled += instance.OnSelectweapon3;
                @Aim.started += instance.OnAim;
                @Aim.performed += instance.OnAim;
                @Aim.canceled += instance.OnAim;
                @Menu.started += instance.OnMenu;
                @Menu.performed += instance.OnMenu;
                @Menu.canceled += instance.OnMenu;
            }
        }
    }
    public GameActions @Game => new GameActions(this);
    public interface IGameActions
    {
        void OnJump(InputAction.CallbackContext context);
        void OnMove(InputAction.CallbackContext context);
        void OnShoot(InputAction.CallbackContext context);
        void OnSelectweapon1(InputAction.CallbackContext context);
        void OnSelectweapon2(InputAction.CallbackContext context);
        void OnSelectweapon3(InputAction.CallbackContext context);
        void OnAim(InputAction.CallbackContext context);
        void OnMenu(InputAction.CallbackContext context);
    }
}
