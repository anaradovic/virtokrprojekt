﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Projectile : MonoBehaviour
{
    public Vector3 direction;
    public int type;
    public GameObject owner;
    private Vector3 startPos;
    private double _damageMultiplier;
    
    private static string[] prefabNames = new [] {
        "BulletPrefab", "LaserPrefab", "RocketPrefab"
    };
    
    private static string[] explosionNames = new [] {
        "BulletExplosion", "LaserExplosion", "RocketExplosion"
    };

    public static void Create(GameObject owner, Vector3 direction, int type, double damageMultiplier)
    {
        var obj = Instantiate(Resources.Load(prefabNames[type], typeof(GameObject))) as GameObject;
        obj.transform.position = owner.transform.position + direction.normalized;
        obj.transform.rotation = Quaternion.FromToRotation(Vector3.up, direction);
        obj.GetComponent<BoxCollider>().isTrigger = true;
        obj.AddComponent<Rigidbody>().useGravity = false;

        var projectileComponent = obj.AddComponent<Projectile>();
        projectileComponent.direction = direction;
        projectileComponent.type = type;
        projectileComponent.owner = owner;
        projectileComponent._damageMultiplier = damageMultiplier;
    }
    
    void Start()
    {
        startPos = gameObject.transform.position;
        direction.Normalize();
    }

    void Update()
    {
        Vector3 pos = gameObject.transform.position;
        pos += direction * Time.deltaTime * Weapons.Speed[type];
        gameObject.transform.position = pos;

        if (Vector3.Magnitude(pos - startPos) > 30)
        {
            Destroy(gameObject);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == owner) return;
        if (other.GetComponent<Projectile>() != null) return;

        Actor actor = other.gameObject.GetComponent<Actor>();
        if (actor != null)
        {
            actor.DoDamage(Weapons.Damage[type] * _damageMultiplier);
            
            if (type != 0)
            {
                var res = Resources.Load(explosionNames[type], typeof(GameObject));
                if (res)
                {
                    var exp = Instantiate(res) as GameObject;
                    exp.transform.position = other.transform.position;
                    Destroy(exp, 2.0f);
                }
            }
        }

        Destroy(gameObject);
    }
}
