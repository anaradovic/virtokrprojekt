﻿using UnityEngine;

public class FollowCamera : MonoBehaviour
{

    /**
     * Object which camera should follow.
     */
    [SerializeField] private GameObject player;
    
    /**
     * Distance which player can move before the camera starts following. Distance between the player and camera objects
     * along the X and Y axis in game units.
     */
    [SerializeField] private float holdDistance = 1;

    void Update()
    {
        Vector3 camPos = transform.position;
        Vector3 plPos = player ? player.transform.position : Vector3.zero;
        plPos += Vector3.up * 3.0f;

        Vector3 diff = plPos - camPos;
        diff.z = 0;
        
        float distance = diff.magnitude;
        if (distance <= 0.3f) return;
        if (player != null && distance < holdDistance) return;
        
        float n = player ? (distance - holdDistance) / holdDistance : distance / holdDistance;
        
        transform.position = camPos + diff * n * Time.deltaTime;
    }
}
