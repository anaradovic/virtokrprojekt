﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour
{
    public const int NumWeapons = 3;

    public static float[] ReloadTime = new float[NumWeapons] {0.6f, 0.3f, 1.0f};
    public static float[] Speed = new float[NumWeapons] {15f, 20f, 10f};
    public static int[] Damage = new int[NumWeapons] {20, 10, 30};

    public double damageMultiplier = 1.0;
    [SerializeField] private int[] ammo = new int[NumWeapons];
    [SerializeField] private int selected = 0;
    private float[] Cooldown = new float[NumWeapons];
    public int Selected => selected;

    public void Shoot(Vector3 direction)
    {
        if (ammo[selected] <= 0) return;
        if (Cooldown[selected] > 0) return;
        
        ammo[selected]--;
        Cooldown[selected] = ReloadTime[selected];
        
        Projectile.Create(gameObject, direction, selected, damageMultiplier);
    }

    public int GetAmmo(int i)
    {
        if (i < 0 || i >= NumWeapons) return -1;
        return ammo[i];
    }

    public void AddAmmo(int i, int amount)
    {
        if (i < 0 || i >= NumWeapons) return;
        ammo[i] += amount;
    }

    public void SetAmmo(int i, int amount)
    {
        if (i < 0 || i >= NumWeapons) return;
        ammo[i] = amount;
    }

    public void Clear()
    {
        ammo = new int[NumWeapons];
        selected = 0;
    }

    public void Select(int i)
    {
        if (i < 0 || i >= NumWeapons) return;
        selected = i;
    }

    private void Update()
    {
        for (int i = 0; i < NumWeapons; i++)
        {
            Cooldown[i] = Math.Max(0.0f, Cooldown[i] - Time.deltaTime);
        }
    }
}