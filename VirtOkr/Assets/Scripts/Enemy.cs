﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private static Type[] required = new []{
        typeof(Actor)
    };

    public int weight = 10;
    public int cost = 10;
    public Actor player = null;
    public string enemyName;
    [SerializeField] private float shootDistance = 3.0f;
    [SerializeField] private float seeDistance = 6.0f;
    [SerializeField] private LayerMask groundLayer = 8;
    [SerializeField] private bool patroler = true;
    [SerializeField] private String inputMap = "null";
    [SerializeField] private float seekingSpeedXMultiplier = 1;
    [SerializeField] private float seekingSpeedYMultiplier = 1;
    [SerializeField] private int timeInervalNextPosition = 200;
    [SerializeField] private int timeInervalNewSearch = 800;
    [SerializeField] private float chasingSpeedX = 0.5f;
    [SerializeField] private float chasingSpeedY = 0.5f;
    [SerializeField] private float retreatingDistacneMultiplier = 0.2f;
    [SerializeField] private float patrolingSpeedX = 1;
    [SerializeField] private int mapWidth = 40;
    [SerializeField] private int mapHeight = 20;
    
    private int patrolingDistance = 10;
    private float hoverHeight = 1.8f;
    private float patrolingSpeed = 0.5f;
    private float descendingSpeed = -0.5f;
    
    private Vector3 homePosition;
    private bool mastGoHome;
    private bool mustPatrol;
    private float patrolingDirection;
    private Vector3 leftPatrolingPosition;
    private Vector3 rigthPatrolingPosition;

    private List<Vector3> platformMap;
    private AStarSearch starSearch;
    private bool aStarCalled;
    private List<Vector3> pathToPosition;

    private DateTime deltaTime;

    private Actor actor;
    
    private void Awake()
    {
        foreach (Type component in required)
        {
            if (GetComponent(component) == null)
            {
                gameObject.AddComponent(component);
            }   
        }

        if (player == null)
        {
            GameObject playerObject = GameObject.Find("Player");
            if (playerObject)
            {
                player = playerObject.GetComponent<Actor>();
            }
        }
        

        actor = GetComponent<Actor>();

        SetHomePosition();
        
        SetMap();

        starSearch = new AStarSearch(platformMap, mapWidth, mapHeight); 
        
        pathToPosition = new List<Vector3>();
        
        deltaTime = new DateTime();

        actor.disableJump();
        GetComponent<Rigidbody>().useGravity = false;
    }

    public void OnDeath()
    {
        Pickup pickup = GetComponent<Pickup>();
        if (pickup != null)
        {
            pickup.Drop();
        }
    }

    private void FixedUpdate()
    {
        if (player == null) return;
        GetComponent<Rigidbody>().useGravity = false;

        Vector3 directionToPlayer = Vector3.Normalize(player.transform.position - transform.position);
        float distaceFromPlayer = Vector3.Magnitude(player.transform.position - transform.position);

        RaycastHit hit;
        if (distaceFromPlayer <= seeDistance && Physics.Raycast(transform.position, directionToPlayer, out hit, distaceFromPlayer + 0.1f) && hit.collider.gameObject == player.gameObject)
        {   
            Chase(directionToPlayer, hit);
        }
        else
        {
            if(mustPatrol && patroler){
                Patrol();
            }
            else
            {   
                if(patroler)
                {
                    mastGoHome = true;
                    GoToHomePosition();      
                }
                else
                {
                    GoToPosition(player.transform.position);
                }
            }
        }
    }

    
    void GoToPosition(Vector3 NewPosition)
    {
        /* Debug.Log("GoToPosition"); */
        
        if(pathToPosition.Count == 0 && !aStarCalled)
        {
            /* Debug.Log("New destination: " + NewPosition);
            Debug.Log("disX : " + (transform.position.x - NewPosition.x));
            Debug.Log("disY : " + (transform.position.y - NewPosition.y));*/
            
            aStarCalled = true;
            starSearch.NewSearch(transform.position, NewPosition);
            
            pathToPosition = starSearch.FindPath();
            deltaTime = DateTime.Now;
            
           /*  if(path.Count != 0)
            {
                foreach(Vector3 vec in path)
                {
                    Debug.Log("toPlayer: " + vec);
                }   
            } */
           
        }
        else
        {
            if(pathToPosition.Count != 0 && IsTime(timeInervalNextPosition))
            {
                Vector3 NextPosition = pathToPosition.First();
                
                pathToPosition.Remove(NextPosition);

                Vector3 directionToPosition = Vector3.Normalize(NextPosition - transform.position);
                
                /*Debug.Log("Xdir: " + directionToPosition.x);
                Debug.Log("Ydir: " + directionToPosition.y);
                Debug.Log("time: " + dt); */

                actor.XMove(directionToPosition.x * seekingSpeedXMultiplier);
                
                actor.YMove(directionToPosition.y * seekingSpeedYMultiplier);

                actor.Aim(directionToPosition);

                /* if(PositionReach(Helpposition))
                {
                    astarcalled = false;
                } */
              

            }
            else if(pathToPosition.Count == 0 && IsTime(timeInervalNewSearch))
            {
                aStarCalled = false;
            }

        }
   
    }

    bool IsTime(int mills)
    {
        /* Debug.Log("IsTime"); */
        
        long millisecondsTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        long millisecondsdt = deltaTime.Ticks / TimeSpan.TicksPerMillisecond;
        
        //Debug.Log("time: " + (millisecondsTime - millisecondsdt));
        
        if(Math.Abs(millisecondsTime - millisecondsdt) > mills)
        {
            deltaTime = DateTime.Now;
            return true;
        }
        return false;
    }
   
 
    void Patrol()
    {   
        /* Debug.Log("Patrol"); */
            
        if(IsGrounded())
        {
            float yDirection = -0.05f;
            
            if(leftPatrolingPosition.Equals(Vector3.zero) || rigthPatrolingPosition.Equals(Vector3.zero))
            {
                SetPatrolingPositions();
                SetHomePosition();
                actor.YMove(0);
            }
            
            if (IsLeftClimbableWall() || IsRightClimbableWall())
            {
                yDirection = 1.5f;

            }

            if((!IsGroundRight() || IsWallRight() || PositionReached(rigthPatrolingPosition)) && patrolingDirection > 0)
            {
                if (IsWallRight() && IsRightClimbable())
                {
                    yDirection = 1.5f;
                }
                else
                {
                    patrolingDirection = -1;
                    actor.Aim(Vector3.left);   
                    actor.YMove(0);
                    yDirection = 0;
                }
            }

            if((!IsGroundLeft() || IsWallLeft() || PositionReached(leftPatrolingPosition)) && patrolingDirection < 0){
                    
                if (IsWallLeft() && IsLeftClimbable())
                {
                    yDirection = 1.5f;
                }
                else
                {
                    patrolingDirection = 1;
                    actor.Aim(Vector3.left);   
                    actor.YMove(0);
                    yDirection = 0;
                }
                    
            }

            if ((IsWallRight() && patrolingDirection > 0) || (IsWallLeft() && patrolingDirection < 0))
            {
                actor.XMove(patrolingDirection * 0.1f);
            }
            else
            {
                actor.XMove(patrolingDirection * patrolingSpeed);
            }

            if (IsLowFlying())
            {
                actor.YMove(Math.Max(0.1f, yDirection));
            }
            else
            {
                actor.YMove(yDirection);
            }
        }
        else
        {
            actor.YMove(descendingSpeed);
        }
    }


    void Chase( Vector3 directionToPlayer, RaycastHit hit)
    {   
        /* Debug.Log("Chase"); */

        mustPatrol = false;
        
        pathToPosition.Clear();
            
        actor.Aim(directionToPlayer);

            if (hit.distance <= shootDistance)
            {
                actor.Shoot();

                if (hit.distance <= shootDistance * retreatingDistacneMultiplier)
                {
                    actor.XMove(directionToPlayer.x > 0 ? -chasingSpeedX : chasingSpeedX);
                    actor.YMove(directionToPlayer.y > 0 ? -chasingSpeedY : chasingSpeedY);
                }
                else
                {
                    actor.XMove(0);
                    actor.YMove(0);
                }
            }
            else
            {
                actor.XMove(directionToPlayer.x > 0 ? chasingSpeedX : -chasingSpeedX);
                actor.YMove(directionToPlayer.y > 0 ? chasingSpeedY : -chasingSpeedY);
            }
        
    }

    void GoToHomePosition()
    {   
        /* Debug.Log("GoToHomePosition"); */
        if(homePosition != Vector3.zero)
        {
            GoToPosition(homePosition); 
            
            if(PositionReached(homePosition))
            {
                /* Debug.Log("Home"); */
                pathToPosition.Clear();
                
                mastGoHome = false;
                mustPatrol = true;
                patrolingDirection = patrolingSpeedX;
            }
        }
        
    }

    void SetMap()
    {
        /*Debug.Log("SetMap"); */
        platformMap = new List<Vector3>();
        if(inputMap != "null")
        {   
            string text = File.ReadAllText(inputMap);
            /* Debug.Log(text); */
            
            for(int i = 0; i < mapWidth; ++i)
            {
                for(int j = 0; j < mapHeight; ++j)
                {
                    if(text[i*mapHeight + j] == 1)
                    {
                        Vector3 NewVector = new Vector3(i, j, 0);
                        if(!platformMap.Contains(NewVector))
                            {
                                platformMap.Add(NewVector);
                            }
                    }
                }
            }
        }  
    }

    bool PositionReached(Vector3 position)
    {
        return Math.Abs(transform.position.x - position.x) < 0.5f;
    }

    void SetHomePosition()
    {
        homePosition = transform.position;
    }
    void SetPatrolingPositions()
    {
        leftPatrolingPosition = transform.position + new Vector3(-patrolingDistance, 0, 0);
        rigthPatrolingPosition = transform.position + new Vector3(patrolingDistance, 0, 0);
    }
    bool IsGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, hoverHeight, groundLayer)
               || Physics.Raycast(transform.position + new Vector3(0.8f, 0, 0), Vector3.down, hoverHeight, groundLayer)
               || Physics.Raycast(transform.position + new Vector3(-0.8f, 0, 0), Vector3.down, hoverHeight, groundLayer);
    }

    bool IsLowFlying()
    {
        return Physics.Raycast(transform.position, Vector3.down, 0.4f, groundLayer)
               || Physics.Raycast(transform.position + new Vector3(0.8f, 0, 0), Vector3.down, 1.0f, groundLayer)
               || Physics.Raycast(transform.position + new Vector3(-0.8f, 0, 0), Vector3.down, 1.0f, groundLayer);
    }
    bool IsGroundLeft()
    {
        return Physics.Raycast(transform.position + new Vector3(-1, 0, 0), Vector3.down, hoverHeight + 1.1f, groundLayer);
    }
    bool IsGroundRight()
    {
        return Physics.Raycast(transform.position + new Vector3(1, 0, 0), Vector3.down, hoverHeight + 1.1f, groundLayer);
    }

    bool IsWallLeft()
    {
        return Physics.Raycast(transform.position + new Vector3(0, -0.2f, 0), Vector3.left, 1.5f, groundLayer);
    }
    bool IsWallRight()
    {
        return Physics.Raycast(transform.position + new Vector3(0, -0.2f, 0), Vector3.right, 1.5f, groundLayer);
    }

    bool IsRightClimbableWall()
    {
        Debug.DrawRay(transform.position, Vector3.right * 2.0f, Color.blue);
        Debug.DrawRay(transform.position, Vector3.Normalize(Vector3.right * 2.0f +  Vector3.up * 1.1f) * 2.3f, Color.blue);
        
        return Physics.Raycast(transform.position, Vector3.right, 2, groundLayer)
            && !Physics.Raycast(transform.position, Vector3.Normalize(Vector3.right * 3.0f +  Vector3.up * 1.1f), 3.2f, groundLayer);
    }

    bool IsLeftClimbableWall()
    {
        return Physics.Raycast(transform.position + new Vector3(0, -0.5f, 0), Vector3.left, 2, groundLayer) 
            && !Physics.Raycast(transform.position + new Vector3(0, -0.5f, 0), Vector3.Normalize(Vector3.left * 3.0f +  Vector3.up * 1.1f), 3.2f, groundLayer);
    }

    bool IsRightClimbable()
    {
        return !Physics.Raycast(transform.position + new Vector3(0, -0.2f, 0), Vector3.Normalize(Vector3.right * 1.5f + Vector3.up * 1.3f),  2, groundLayer);
    }
    
    bool IsLeftClimbable()
    {
        return !Physics.Raycast(transform.position + new Vector3(0, -0.2f, 0), Vector3.Normalize(Vector3.left * 1.5f + Vector3.up * 1.3f),  2, groundLayer);
    }
}