﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarScript : MonoBehaviour
{
    public Slider slider;
    public Gradient gradient;

    public Image fill;

    public void Start() 
    {
        slider.maxValue = 100;
        slider.value = 100;

        fill.color = gradient.Evaluate(1f);
    }

    public void SetHealth(double health)
    {
        slider.value = (int) Math.Round(health);

        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
