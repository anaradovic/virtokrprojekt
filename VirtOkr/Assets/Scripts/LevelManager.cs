﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;


public class LevelManager : MonoBehaviour
{
    // public bool runInEditMode;
    public GameObject wallElement;
    public int xRange;
    public int yRange;
    public EnemyDirector enemyDirector;
    private List<GameObject> _wallInstance = new List<GameObject>();
    private Dictionary<string, List<int>> _levelList = new Dictionary<string, List<int>>();
    private ArrayList _levelNames = new ArrayList();
    private ArrayList _levelsVisited = new ArrayList();
    private List<WallComponent> _spawnerList = new List<WallComponent>();
    private List<GameObject> currentEnemies = new List<GameObject>();
    private int currentLevel = 0;
    public TextMeshProUGUI levelChange = null;
    public TextMeshProUGUI levelChangeTime = null;
    public Player player;

    // Start is called before the first frame update
    void Start()
    {
        levelChange.enabled = levelChangeTime.enabled = false;
        LoadLevels();
        for (int i = 0; i < yRange; i++)
        {
            for (int j = 0; j < xRange; j++)
            {
                GameObject tempWall = Instantiate(wallElement,new Vector3((float)(-9.5 + j), i, (float)0.5), Quaternion.identity, transform);
                tempWall.GetComponent<WallComponent>().SetTweenReference(tempWall);
                _wallInstance.Add(tempWall);
            }
        }

        int starterCnt = -1;
        foreach (string s in _levelNames)
        {
            if (s.StartsWith("starter")){
                starterCnt++;
            }
        }

        int startNum = Random.Range(0, starterCnt);
        string key = "starter" + startNum;
        List<int> selLevel = _levelList[key];
        StartCoroutine(ChangeLevel(level:selLevel, waitTime:0));
    }

    GameObject findClosest()
    {
        GameObject closest = null;

        foreach (var wall in _wallInstance)
        {
            if (wall.GetComponent<WallComponent>()._alive) continue;
            if(wall.transform.position.z - player.transform.position.z < 0.3) continue;
            
            if (closest == null)
            {
                closest = wall;
                continue;
            }

            if (Vector3.Distance(wall.transform.position, player.transform.position) <
                Vector3.Distance(closest.transform.position, player.transform.position))
            {
                closest = wall;
            }
        }
        
        return closest;
    }

    // Changes levels from one configuration to another. Can be given a level as parameter but is only used
    // for first configuration
    IEnumerator ChangeLevel(List<int> level = null, float waitTime = 10){
        for (var i = 0; i < currentEnemies.Count; i++) if(currentEnemies[i]) Destroy(currentEnemies[i]);
        currentEnemies.Clear();

        string pickedLevel;
        List<int> levelData;
        _spawnerList.Clear();
        if (level == null){
            while (true){
                pickedLevel = (string) _levelNames[Random.Range(0, _levelNames.Count)];
                if (!_levelsVisited.Contains(pickedLevel)){
                    _levelsVisited.Add(pickedLevel);
                    break;
                }
            }
            levelData = _levelList[pickedLevel];
        } else {
            levelData = level;
        }

        if (waitTime > 4)
        {
            var remaining = waitTime - 4;
            waitTime = 4;
            yield return new WaitForSeconds(remaining);
        }

        for(int i = 0; i < 800; i++){
            GameObject gObject = _wallInstance[i];
            WallComponent wall = gObject.GetComponent<WallComponent>();
            wall.SetState((short)levelData[i]);
            if (levelData[i] != 0 && levelData[i] != 2)
            {
                wall.SetPreview();
            }
        }

        while (waitTime > 0)
        {
            levelChangeTime.text = (waitTime-1).ToString();
            levelChange.enabled = levelChangeTime.enabled = true;
            waitTime--;
            yield return new WaitForSeconds(1);
        }
        levelChange.enabled = levelChangeTime.enabled = false;

        var closestWall = findClosest();

        var spawnPoints = new List<GameObject>();
        for(int i = 0; i < 800; i++){
            GameObject gObject = _wallInstance[i];
            WallComponent wall = gObject.GetComponent<WallComponent>();
            if (levelData[i] == 0){
                wall.SetInactive();
            } else if(levelData[i] == 1){
                wall.SetActive();
            } else {
                wall.SetActive();
                _spawnerList.Add(wall);
                spawnPoints.Add(gObject);
            }
        }

        if(_levelsVisited.Count == _levelNames.Count){
            _levelsVisited.Clear();
        }

        if (closestWall && closestWall.GetComponent<WallComponent>()._alive)
        {
            var actor = player.GetComponent<Actor>();
            actor.disableJump();
            actor.unlock();
        }

        yield return new WaitForSeconds(1.5f);
        currentLevel++;
        currentEnemies = enemyDirector.SpawnEnemies(spawnPoints, currentLevel);
    }
    
    // Loads all levels (saved as text files) from Scenes\Levels
    void LoadLevels()
    {

        string[] levels = {"Levels/starter0", "Levels/starter1", "Levels/starter2", "Levels/starter3", "Levels/starter4", "Levels/normal0", "Levels/normal1", "Levels/normal2", "Levels/normal3", "Levels/normal4", "Levels/normal5"};
        foreach (string level in levels)
        {
            List<int> levelData = new List<int>();
            var textFile = Resources.Load<TextAsset>(level);
            string tempLevel = textFile.text;
            foreach (char c in tempLevel){
                levelData.Add((int)char.GetNumericValue(c));
            }
            levelData.Reverse();
            string lName = level.TrimEnd('.', 't', 'x');
            _levelNames.Add(lName.Remove(0, 7));
            _levelList.Add(lName.Remove(0, 7), levelData);
        }

    }

    public List<WallComponent> GetSpawners()
    {
        return _spawnerList;
    }

    private void Update()
    {
        if(currentEnemies.Count != 0){
            bool allKilled = true;
            foreach (var currentEnemy in currentEnemies)
            {
                if (currentEnemy) allKilled = false;
                if (!allKilled) break;
            }

            if (allKilled)
            {
                currentEnemies.Clear();
                StartCoroutine(ChangeLevel(level:null, 4.0f + (float) Math.Round(4.0f/ (float) (Math.Max(2.0f, currentLevel / 2.0f)))));
            }
        }
    }
}
