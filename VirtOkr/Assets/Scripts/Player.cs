﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    private static Type[] required = {
        typeof(Actor),
        typeof(PlayerInput),
        typeof(Weapons)
    };

    public HealthBarScript bar;

    private TextMeshProUGUI[] ammoText = new TextMeshProUGUI[Weapons.NumWeapons];
    private TextMeshProUGUI healthText = null;
    private Weapons weapons;
    private Actor actor;
    private PlayerInput input;

    private static TextMeshProUGUI FindTextElement(String name)
    {
        GameObject textObject = GameObject.Find(name);
        if(textObject == null) return null;
        return textObject.GetComponent<TextMeshProUGUI>();
    }

    private void Awake()
    {
        foreach (Type component in required)
        {
            if (GetComponent(component) == null)
            {
                gameObject.AddComponent(component);
            }   
        }

        for (int i = 0; i < Weapons.NumWeapons; i++)
        {
            ammoText[i] = FindTextElement("Weapon" + (i + 1));
        }
        healthText = FindTextElement("Health");

        weapons = GetComponent<Weapons>();
        actor = GetComponent<Actor>();
        input = GetComponent<PlayerInput>();
    }

    public void OnDeath()
    {
        UpdateUI();
        
        //dodati tamo gdje se gleda je li health playera na nuli
        //dodati endText i staviti enimy counter
        //endText.Set("Congratulations, you've successfully conquered " + enemyCounter + " enemies!");
        SceneManager.LoadScene("GameOverScene", LoadSceneMode.Single);
    }

    private void UpdateUI()
    {
        if (ammoText[0] == null)
        {
            Debug.Log("UpdateUI: ammo is null");
            return;
        }

        for (int i = 0; i < Weapons.NumWeapons; i++)
        {
            if (ammoText[i] == null) continue;
            ammoText[i].SetText(weapons.GetAmmo(i).ToString());
            ammoText[i].faceColor = weapons.Selected == i ? new Color32(255, 200, 200, 255) : new Color32(200, 200, 200, 255);
        }

        if (bar)
        {
            bar.SetHealth(actor.health);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        UpdateUI();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateUI();
    }
}
