﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameController : MonoBehaviour
{

    public void playClicked()
    {
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }

    public void replayClicked() {
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }

    public void quitClicked() {
        Application.Quit();
    }
}
