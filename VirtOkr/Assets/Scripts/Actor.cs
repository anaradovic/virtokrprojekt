using System;
using System.Numerics;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.UI;
using Vector3 = UnityEngine.Vector3;

public class Actor : MonoBehaviour
{
    /**
     * Max floor distance.
     */
    [SerializeField] private float floorDistance = 1f;
    
    /**
     * Max floor angle cos.
     */
    [SerializeField] private float floorAngle = 0.7f;
    
    /**
     * Velocity vector amplitude.
     */
    [SerializeField] private float jumpIntensity = 7;
    [SerializeField] private float actorVelocity = 5f;
    private Rigidbody rigid;

    private bool jumpDisabled = false;
    private bool unlocked = false;
    
    private float movementDirection;
    private float YmovementDirection;
    private bool isGrounded;
    private Vector3 groundNormal;
    private bool touchingWall;
    private Vector3 wallNormal;
    private bool jump;
    private bool doubleJump;

    private Weapons weapons = null;
    public Weapons Weapons => weapons;

    private Vector3 aim;
    private GameObject aimIndicator = null;

    [SerializeField] public double health = 100.0;
    public double Health => health;
    private double _initialHealth;
    
    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        rigid.constraints |= RigidbodyConstraints.FreezePositionZ;

        weapons = GetComponent<Weapons>();
        
        movementDirection = 0;
        isGrounded = false;
        groundNormal = Vector3.up;
        jump = false;
        doubleJump = false;
        _initialHealth = health;
        
        /*aimIndicator = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        aimIndicator.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        Destroy(aimIndicator.GetComponent<SphereCollider>());*/
    }

    public void unlock()
    {
        unlocked = true;
        rigid.constraints &= ~RigidbodyConstraints.FreezePositionZ;
        rigid.constraints |= RigidbodyConstraints.FreezePositionX;
        rigid.constraints |= RigidbodyConstraints.FreezePositionY;
    }
    
    public void disableJump()
    {
        jumpDisabled = true;
    }

    private void OnDestroy()
    {
        if (aimIndicator)
        {
            Destroy(aimIndicator);
        }
    }

    public void Start()
    {
        Aim(Vector3.right);
    }

    public void Jump()
    {
        if (jumpDisabled) return;
        
        Vector3 velocity = rigid.velocity;
        
        if (!isGrounded && !touchingWall)
        {
            if (doubleJump) return;

            if (movementDirection == 0)
            {
                velocity += Vector3.up * jumpIntensity;
            }else if (movementDirection < 0)
            {
                velocity += Vector3.Normalize(Vector3.up + Vector3.left) * jumpIntensity * 1.5f;
            }else if (movementDirection > 0)
            {
                velocity += Vector3.Normalize(Vector3.up + Vector3.right) * jumpIntensity * 1.5f;
            }

            rigid.velocity = velocity;
            doubleJump = true;
            return;
        }
        if (jump) return;

        if (isGrounded)
        {
            velocity += groundNormal * jumpIntensity;
        }else if (touchingWall)
        {
            velocity += Vector3.Normalize(wallNormal + Vector3.up * 2.0f) * jumpIntensity;
        }
        
        rigid.velocity = velocity;
        jump = true;
    }

    public void Move(float direction)
    {
        movementDirection = direction;
    }

    public void XMove(float direction)
    {
        Move(direction);
    }

    public void YMove(float direction)
    {
        YmovementDirection = direction;
    }
    
    public void Shoot()
    {
        if (weapons == null) return;
        weapons.Shoot(aim);
    }

    public void Aim(Vector3 dir)
    {
        aim = dir;
        aim.Normalize();
        
        if (aimIndicator)
        {
            aimIndicator.transform.position = transform.position + aim * 1.5f;    
        }
    }

    public void SelectWeapon(int i)
    {
        if (weapons == null) return;
        weapons.Select(i);
    }

    public void DoDamage(double value)
    {
        health = Math.Max(0.0, health - value);

        if (Math.Round(health) == 0.0)
        {
            Kill();
        }
    }

    public void Kill()
    {
        if (weapons) weapons.Clear();
        
        Enemy enemy = GetComponent<Enemy>();
        if(enemy) enemy.OnDeath();
        
        Player player = GetComponent<Player>();
        if(player) player.OnDeath();

        Destroy(gameObject);
    }

    public void PickUp(Pickup pickup)
    {
        for (int i = 0; i < Weapons.NumWeapons; i++)
        {
            Weapons.AddAmmo(i, pickup.ammo[i]);
        }

        health = Math.Min(_initialHealth, health + pickup.health);
    }

    private void Update()
    {
        if (aimIndicator)
        {
            aimIndicator.transform.position = transform.position + aim * 1.5f;   
        }

         if (transform.position.y <= -3)
         {
             Kill();
         }
    }

    struct RayTest
    {
        public Vector3 dir;
        public float dis;
        public Vector3 origin;
    }

    private Vector3 checkCollistion(RayTest[] tests, bool angleCheck)
    {
        bool grounded = false;
        Vector3 normal = Vector3.up;
        foreach(var test in tests)
        {
            RaycastHit hit;
            Debug.DrawRay(transform.position, test.dir.normalized * test.dis, Color.red);
            if (Physics.Raycast(transform.position, test.dir, out hit, test.dis))
            {
                Vector3 n = Vector3.Normalize(hit.normal);
                float cos = Vector3.Dot(n, Vector3.up);
                // if (cos < 0) return; // bottom surface
                grounded = !angleCheck || cos >= floorAngle;
                if (grounded)
                {
                    normal = n;
                    Debug.DrawRay(transform.position, test.dir.normalized * test.dis, Color.green);
                    break;
                }
            }
        }

        if (grounded) return normal;
        return Vector3.zero;
    }

    private void FixedUpdate()
    {
        // floor tests
        groundNormal = checkCollistion(new []{
            new RayTest { dir = Vector3.down, dis = floorDistance },
            new RayTest { dir = Vector3.Normalize(Vector3.down + Vector3.left), dis = floorDistance * 1.5f },
            new RayTest { dir = Vector3.Normalize(Vector3.down + Vector3.right), dis = floorDistance * 1.5f }
        }, true);
        isGrounded = groundNormal.magnitude != 0;
        
        // wall tests
        wallNormal = checkCollistion(new []{
            new RayTest { dir = Vector3.left, dis = 0.5f },
            new RayTest { dir = Vector3.right, dis = 0.5f },
            new RayTest { dir = Vector3.Normalize(Vector3.left + Vector3.up * 0.6f), dis = 0.5f * 1.4f },
            new RayTest { dir = Vector3.Normalize(Vector3.left + Vector3.down * 0.6f), dis = 0.5f * 1.4f },
            new RayTest { dir = Vector3.Normalize(Vector3.right + Vector3.up * 0.6f), dis = 0.5f * 1.4f },
            new RayTest { dir = Vector3.Normalize(Vector3.right + Vector3.down * 0.6f), dis = 0.5f * 1.4f },
        }, false);
        touchingWall = wallNormal.magnitude != 0;

        bool hangingLeft = false;
        bool hangingRight = false;
        if (touchingWall)
        {
            hangingLeft = Vector3.Angle(wallNormal, Vector3.right) < 0.1;
            hangingRight = Vector3.Angle(wallNormal, Vector3.left) < 0.1;
        }
        
        // movement
        if (YmovementDirection != 0)
        {
            Vector3 velocity = rigid.velocity;
            velocity.y = YmovementDirection * actorVelocity;
            rigid.velocity = velocity;
        }
         
        if (jumpDisabled && movementDirection != 0)
        {
            Vector3 velocity = rigid.velocity;
            velocity.x = movementDirection * actorVelocity;
            rigid.velocity = velocity;
        }
        
        if (jumpDisabled) return;

        // movement + wall climbing
        if (movementDirection != 0)
        {

            Vector3 velocity = rigid.velocity;
            if (movementDirection < 0 && !hangingLeft || movementDirection > 0 && !hangingRight)
            {
                velocity.x = movementDirection * actorVelocity;
                rigid.constraints &= ~RigidbodyConstraints.FreezePositionX;
                rigid.constraints &= ~RigidbodyConstraints.FreezePositionY;
            }else if (movementDirection > 0 && hangingLeft || movementDirection < 0 && hangingRight)
            {
                velocity.x = 0;
                velocity.y = 0;
                rigid.constraints |= RigidbodyConstraints.FreezePositionX;
                rigid.constraints |= RigidbodyConstraints.FreezePositionY;
            }
            else
            {
                rigid.constraints &= ~RigidbodyConstraints.FreezePositionX;
                rigid.constraints &= ~RigidbodyConstraints.FreezePositionY;
            }
            rigid.velocity = velocity;
        }
        
        // wall physics
        if (touchingWall && rigid.velocity.y < 0)
        {
            Vector3 velocity = rigid.velocity;
            velocity.y = 0;
            rigid.velocity = velocity;
        }
        
        // jump
        if (jump && rigid.velocity.y <= 0)
        {
            jump = false;
        }
        
        if (touchingWall || isGrounded)
        {
            doubleJump = false;
        }
    }
}