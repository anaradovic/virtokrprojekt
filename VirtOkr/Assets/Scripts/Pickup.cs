﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{

    [SerializeField] public int[] ammo = new int[Weapons.NumWeapons];
    [SerializeField] public int health = 0;

    private float dropTime = 0;
    private Vector3 pos;
    private int state = 0;
    private float verticalVelocity = 0;

    public void Drop()
    {
        GameObject obj = Instantiate((GameObject) Resources.Load("PickupPrefab", typeof(GameObject)));
        obj.transform.position = transform.position + new Vector3(0, -0.3f, 0);
        obj.GetComponent<BoxCollider>().isTrigger = true;

        Pickup pickup = obj.AddComponent<Pickup>();
        pickup.ammo = ammo;
        pickup.health = health;

        Destroy(this);
    }
    

    void Start()
    {
        if (GetComponent<Actor>() != null)
        {
            enabled = false;
            return;
        }

        pos = transform.position;
    }

    void Update()
    {
        if (transform.position.y <= -3)
        {
            Destroy(gameObject);
            return;
        }
        
        Vector3 rot = transform.eulerAngles;
        rot.y += Time.deltaTime * 90.0f;
        if (rot.y >= 360)
        {
            rot.y -= 360.0f;
        }
        gameObject.transform.eulerAngles = rot;

        if (state == 3) return;
        
        if (state == 0)
        {
            dropTime = Math.Min(1, dropTime + Time.deltaTime * 2.5f);
            float ease = (float) (1.0 - Math.Pow(1.0f - dropTime, 3.0f));
            transform.position = pos + ease * Vector3.up / 2.0f;

            if (dropTime == 1)
            {
                dropTime = 0;
                state = 1;
            }
        }else if (state == 1)
        {
            verticalVelocity += -9.8f * Time.deltaTime;
            transform.position += Vector3.up * verticalVelocity * Time.deltaTime;
            
            if (Physics.Raycast(transform.position, Vector3.down, 0.6f))
            {
                state = 2;
                pos = transform.position;
            } 
        }else if (state == 2)
        {
            dropTime = Math.Min(1, dropTime + Time.deltaTime);
            float ease;
            
            const float n1 = 7.5625f;
            const float d1 = 2.75f;

            float t = dropTime * (1 - 1 / d1) + 1 / d1;

            if (t < 1 / d1) {
                ease = n1 * t * t;
            } else if (t < 2 / d1) {
                ease = n1 * (t -= 1.5f / d1) * t + 0.75f;
            } else if (t < 2.5 / d1) {
                ease = n1 * (t -= 2.25f / d1) * t + 0.9375f;
            } else {
                ease = n1 * (t -= 2.625f / d1) * t + 0.984375f;
            }

            ease = 1.0f - ease;

            transform.position = pos + ease * Vector3.up * Math.Abs(verticalVelocity) / 3.0f;

            if (dropTime == 1)
            {
                state = 3;
                dropTime = 0;
                verticalVelocity = 0;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>() == null) return;
        Actor actor = other.GetComponent<Actor>();
        actor.PickUp(this);
        Destroy(gameObject);
    }
}
