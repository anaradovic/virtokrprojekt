﻿using System;
using UnityEngine;

public class WallComponent : MonoBehaviour
{

    public bool _alive;
    private short _state;
    private Vector3 startPos;
    public Material active;
    public Material inactive;
    public Material spawner;
    public Material preview;
    public PotaTween tween;
    // Start is called before the first frame update
    // void Start()
    // {
    //     
    // }

    private bool IsAlive()
    {
        return _alive;
    }

    public void SetTweenReference(GameObject target)
    {
        tween = PotaTween.Create(target);
        startPos = transform.position;
    }

    public void SetState(short state)
    {
        _state = state;
    }
    public void SetActive()
    {
        
        if (_state == 1)
        {
            this.GetComponent<Renderer>().material = active;
            if (!_alive)
            {
                _alive = true;
                tween.SetPosition(startPos, startPos + Vector3.back);
                tween.Play();
            }
        }
        else if (_state == 2)
        {
            _alive = true;
            this.GetComponent<Renderer>().material = spawner;
        }
    }

    public void SetInactive()
    {
        this.GetComponent<Renderer>().material = inactive;
            _alive = false;
            tween.Reset();
            tween.SetPosition(transform.position, startPos);
            tween.Play();
            _state = 0;
    }

    public void SetPreview()
    {
        this.GetComponent<Renderer>().material = preview;
    }
}
