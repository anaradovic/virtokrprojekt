﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Animations;

class Tile
{
    public const float multiplier = 1f;
    public int X, Y;
    public Tile Parent = null;

    public int Cost = 0;
    public int Distance = 0;

    public int CostDistance => Cost + Distance;

    public Tile(Vector3 pos)
    {
        X = (int) Math.Round(pos.x * multiplier);
        Y = (int) Math.Round(pos.y * multiplier);
    }

    public Tile(int x, int y, Tile parent = null, int cost = 0)
    {
        this.X = x;
        this.Y = y;
        this.Parent = parent;
        this.Cost = cost;
    }

    public void SetDistance(Tile other)
    {
        Distance = (int) Math.Sqrt(Math.Pow(other.X - X, 2) + Math.Pow(other.Y - Y, 2));
        //Distance = Math.Abs(other.X - X) + Math.Abs(other.Y - Y);
    }

    public Vector3 ToWorld()
    {
        return new Vector3(X, Y, 0) / multiplier;
    }

    public override int GetHashCode()
    {
        return X * 241 + Y * 5281;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return GetHashCode() == obj.GetHashCode();
    }

    public static bool operator ==(Tile a, Tile b)
    {
        if (ReferenceEquals(a, null) && ReferenceEquals(b, null)) return true;
        if (ReferenceEquals(a, null)) return false;
        return a.Equals(b);
    }

    public static bool operator !=(Tile a, Tile b)
    {
        if (ReferenceEquals(a, null) && ReferenceEquals(b, null)) return false;
        if (ReferenceEquals(a, null)) return true;
        return !(a.Equals(b));
    }
}

public class AStarSearch
{
    private Tile start;
    private Tile finish; 
    private LayerMask layer; 
    private List<Tile> obstacleTileMap;
    private int counter;
    private const int minX = 0;
    private const int minY = 0;
    private int mapWidth = 40;
    private int mapHeight = 20;

    
    public AStarSearch(List<Vector3> obstacleVector3Map,int Width, int Height)
    {

        obstacleTileMap = new List<Tile>();
        mapWidth = Width;
        mapHeight = Height;
        counter = 0;
        SetMap(obstacleVector3Map);
    }

    void SetMap(List<Vector3> vector3Map)
    {
        foreach(Vector3 vec in vector3Map)
        {
            Tile tile = new Tile(vec);
            if(!obstacleTileMap.Contains(tile))
            {
                obstacleTileMap.Add(tile);
            }
        }
    }     

    public void NewSearch(Vector3 startPosition,  Vector3 endPosition)
    {
        start = new Tile(startPosition);
        finish = new Tile(endPosition);
    }

    public List<Vector3> FindPath()
    {
        start.SetDistance(finish);

        var open = new List<Tile>();
        open.Add(start);
        var closed = new List<Tile>();

        while (open.Count != 0 && open.First() != finish)
        {
            counter++;
            Tile current = open.First();
            
            /*Debug.Log("Counter : " + counter);
            Debug.Log("Open Tiles : " + open.Count);
            Debug.Log("Current tile : " + current.ToWorld());*/

            open.Remove(current);
            closed.Add(current);
            var neighbours = GetWalkableTiles(current);
            foreach (var n in neighbours)
            {
                Tile tile;
                if ((tile = open.Find(t => t == n)) != null && n.Cost < tile.Cost)
                {
                    
                    open.Remove(n);
                }
                
                if ((tile = closed.Find(t => t == n)) != null && n.Cost < tile.Cost)
                {
                    closed.Remove(n);
                }

                if (!open.Contains(n) && !closed.Contains(n))
                {
                    open.Add(n);
                }
            }
           
           
            open = open.OrderBy(t => t.CostDistance).ToList();
            
        }

        if (open.Count == 0)
        {
            return new List<Vector3>();   
        }

        return Backtrace(open.First());
    }

    private List<Tile> GetWalkableTiles(Tile currentTile)
    {
        var possibleTiles = new List<Tile>()
        {
            new Tile(currentTile.X, currentTile.Y - 1, currentTile, currentTile.Cost + 1),
            new Tile(currentTile.X, currentTile.Y + 1, currentTile, currentTile.Cost + 1),
            new Tile(currentTile.X - 1, currentTile.Y, currentTile, currentTile.Cost + 1),
            new Tile(currentTile.X + 1, currentTile.Y, currentTile, currentTile.Cost + 1),
        };

        possibleTiles.ForEach(tile => tile.SetDistance(finish));

        List<Tile> walkable = new List<Tile>();
        foreach (Tile tile in possibleTiles)
        {             
            //Debug.Log("Tile" + tile.ToWorld() + " : " + tile.X + ", " + tile.Y);
            if (Math.Abs(tile.X) > mapWidth || Math.Abs(tile.Y) > mapHeight || tile.X < minX || tile.Y < minX)
            {
                continue;
            }

            if(obstacleTileMap.Contains(tile)){
               continue;
            }
            walkable.Add(tile);
        }

        //Debug.Log("Walkable count : " + walkable.Count);
        /* if (walkable.Count != 0)
        {
            Debug.Log(walkable[0].Distance);
        } */
        
        return walkable;
    }

    private List<Vector3> Backtrace(Tile destination)
    {
        List<Vector3> trace = new List<Vector3>();
        Tile current = destination;
        
        while (current != null)
        {
            trace.Add(current.ToWorld());
            current = current.Parent;
        }
      
        trace.Reverse();
        
        return trace;
    }

    
}

