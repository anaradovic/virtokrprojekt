﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class EnemyDirector : MonoBehaviour
{
    public GameObject[] enemyPrefabs;
    public Actor player;
    public int maximumMaxEnemies = 16;
    public int startingLevelMaxEnemies = 2;
    public int maxEnemiesIncrementPerLevel = 2;
    public int startingLevelBudget = 50;
    public int budgetIncrementPerLevel = 25;

    public List<GameObject> SpawnEnemies(IEnumerable<GameObject> spawnPoints, int level = 1)
    {
        var maxEnemies = Math.Min(startingLevelMaxEnemies + (level - 1) * maxEnemiesIncrementPerLevel , maximumMaxEnemies);
        var budget = startingLevelBudget + (level - 1) * budgetIncrementPerLevel;
        var currentSpawnPoints = new List<GameObject>(spawnPoints);
        var ens = new List<GameObject>(enemyPrefabs);
        var spawnedEnemies = new List<GameObject>();
        var interpolationFactor = Math.Tanh(0.01 * level);

        Debug.Log("Starting to spawn enemies. Budget = " + budget + ".");
        for (var i = 0; i < maxEnemies; i++)
        {
            if (currentSpawnPoints.Count == 0)
            {
                Debug.Log("Stopping. No more spawn points.");
                break;
            }
            var canSpawn = new List<GameObject>();
            var totalWeight = 0;
            foreach (var e in ens)
            {
                var enemy = e.GetComponent<Enemy>();
                if (enemy.cost > budget) continue;
                canSpawn.Add(e);
                totalWeight += enemy.weight;
            }

            var chosenWeight = Random.Range(0, totalWeight);
            // Determine chosen player
            GameObject chosenEnemy = null;
            var currentWeight = 0;
            foreach (var e in canSpawn)
            {
                var enemy = e.GetComponent<Enemy>();
                Assert.IsNotNull(enemy);
                currentWeight += enemy.weight;
                if (currentWeight >= chosenWeight)
                {
                    chosenEnemy = e;
                    break;
                }
            }

            if (chosenEnemy == null)
            {
                Debug.Log("Stopping. Can not afford any more enemies.");
                break;
            }
            var en = chosenEnemy.GetComponent<Enemy>();
            budget -= en.cost;
            var chosenSpawnIndex = Random.Range(0, currentSpawnPoints.Count);
            var chosenSpawnPoint = currentSpawnPoints[chosenSpawnIndex];
            var spawnPointPosition = chosenSpawnPoint.transform.position;
            var x = spawnPointPosition.x;
            var y = spawnPointPosition.y;
            en.player = player;
            var spawnedEnemy = Instantiate(chosenEnemy,
                new Vector3(x, y, -1.0F),
                chosenSpawnPoint.transform.rotation);
            spawnedEnemies.Add(spawnedEnemy);

            var enemyComponent = spawnedEnemy.GetComponent<Enemy>();
            Assert.IsNotNull(enemyComponent);
            Debug.Log("Spawning " + enemyComponent.enemyName + " for a cost of " + enemyComponent.cost + ". Current budget = " + budget + ".");
            var actor = spawnedEnemy.GetComponent<Actor>();
            Assert.IsNotNull(actor);
            var pickup = spawnedEnemy.GetComponent<Pickup>();
            Assert.IsNotNull(pickup);
            var weapons = spawnedEnemy.GetComponent<Weapons>();
            Assert.IsNotNull(weapons);

            actor.health += actor.health * interpolationFactor;
            pickup.health += (int)Math.Round(pickup.health * interpolationFactor);
            weapons.damageMultiplier += weapons.damageMultiplier * interpolationFactor;
            for (var j = 0; j < 3; j++)
            {
                pickup.ammo[j] = (int) Math.Round(actor.health / Weapons.Damage[j] * 0.5);
            }
            
            var weapon = Random.Range(0, 3);
            weapons.Select(weapon);
            //weapons.SetAmmo(weapon, 15 + Random.Range(level, level * 3));
            //actor.health = 25 + Random.Range(level * 2, level * 10);
            //pickup.health = 50 * (1 / level) + Random.Range(20, 80 * 1 / level);
            //pickup.ammo[0] = 15 * (1 / level) + Random.Range(0, 15 * 1 / level);
            //pickup.ammo[1] = 15 * (1 / level) + Random.Range(0, 15 * 1 / level);
            //pickup.ammo[2] = 15 * (1 / level) + Random.Range(0, 15 * 1 / level);

            currentSpawnPoints.Remove(chosenSpawnPoint);
        }

        if (spawnedEnemies.Count == maxEnemies)
        {
            Debug.Log("Stopping. Reached maximum number of enemies.");
        }

        return spawnedEnemies;
    }
}